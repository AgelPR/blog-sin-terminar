<?php require_once("includes/verifico_sesion.php");?>
<?php require_once("includes/header.php");?>
<?php require_once("includes/lateral.php");?>

<div id="principal">
  <h1>Nueva Categoria</h1>
  <p>Crea nuevas categorías para el blog para que los usuarios puedan usarlas al crear sus entradas</p>
  <br>

  <form action="guardar_categoria.php" method="POST">
    <label for="newCategoria">Nombre de la categoría</label>
    <input type="text" name="newCategoria">

    <input type="submit" value="Guardar" id="guardarItem">
  </form>
</div>

<?php require_once("includes/footer.php");?>