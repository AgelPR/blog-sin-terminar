<?php
if (isset($_POST)) {
  if (isset($_SESSION['error_login'])) {
    session_unset($_SESSION['error_login']);
  }
  // Conexion.
  require_once("includes/config.php");
  // Obtengo los datos
  $email = isset($_POST['email']) ? $_POST['email'] : false;
  $password = isset($_POST['password']) ? $_POST['password'] : false;
  // Compruebo que existe el usuario
  $query = "SELECT * FROM usuarios WHERE email = '$email' LIMIT 1";
  $result = mysqli_query($link, $query);

  // Compruebo que el resutaldo de la busqueda sea igual a 1
  if ($result && mysqli_num_rows($result) == 1) {
    // Creo la variable para guardar el array de resultado de la bd
    $usuario_logueado = mysqli_fetch_assoc($result);
    // Comprobar la contraseña apoyandome en el arreglo que trae mysqli_fetch_assoc
    $contra_validado = password_verify($password, $usuario_logueado['password']);
    if ($contra_validado == true) {
      // Creo una sesión con el nombre del usuario
      $_SESSION['usuario'] = $usuario_logueado;
    } else {
      // Muestro mensaje de error con una sesion
      $_SESSION['error_login'] = 'Login incorrecto';
    }
  } else {
    $_SESSION['error_login'] = 'Login incorrecto';
  }
}
header("Location:index.php");
