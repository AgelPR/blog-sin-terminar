<?php 
  if (isset($_POST)) {
    require_once("includes/config.php");
    $newCategoria = isset($_POST['newCategoria']) ? mysqli_real_escape_string($link, $_POST['newCategoria']) : false;

    $errores = array();

    if (!empty($newCategoria) && !is_numeric($newCategoria) && !preg_match("/[0-9]/", $newCategoria)) {
      $categoriaValidada = true;
    }else{
      $categoriaValidada = false;
      $errores['categoria'] = "La categoria no puede tener números o carácteres especiales";
    }

    if (count($errores) == 0) {
      $query = "INSERT INTO categorias VALUES (null, '$newCategoria')";
      $result = mysqli_query($link, $query);
    }else{
      $_SESSION['errores_entrada'] = $errores;
    }
    
  }
header("Location: index.php");