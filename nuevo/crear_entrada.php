<?php require_once("includes/verifico_sesion.php"); ?>
<?php require_once("includes/header.php"); ?>
<?php require_once("includes/lateral.php"); ?>

<div id="principal">
  <h1>Nueva Entrada</h1>
  <p>Crea nuevas entrada para el blog de esta manera los usuarios podran ver tu contenido</p>
  <br>

  <form action="guardar_entrada.php" method="POST">
    <label for="newEntrada">Titulo</label>
    <input type="text" name="newEntrada">
    <?php echo isset($_SESSION['errores_entrada']) ? mostrarError($_SESSION['errores_entrada'], 'titulo') : ''; ?>

    <label for="newDescripcion">Descripción</label>
    <textarea name="newDescripcion" cols="30" rows="10"></textarea>
    <?php echo isset($_SESSION['errores_entrada']) ? mostrarError($_SESSION['errores_entrada'], 'descripcion') : ''; ?>

    <label for="categoria">Categorías</label>
    <?php echo isset($_SESSION['errores_entrada']) ? mostrarError($_SESSION['errores_entrada'], 'categoria') : ''; ?>
    <div class="select">
      <select name="categoria">
        <?php
        $categorias = conseguirCategorias($link);
        if (!empty($categorias)) :
          while ($categoria = mysqli_fetch_assoc($categorias)) : ?>

        <option value="<?= $categoria['id'] ?>"><?= $categoria['nombre'] ?></option>

        <?php
          endwhile;
        endif;
        ?>
      </select>
    </div>
    <br>
    <input type="submit" value="Guardar" id="guardarItem">
  </form>
</div>

<?php require_once("includes/footer.php"); ?>