<?php require_once("includes/header.php") ?>
<!-- CONTENIDO PRINCIPAL -->

<?php require_once("includes/lateral.php") ?>
<!-- CAJA PRINCIPAL -->
<div id="principal">
  <h1>Últimas entradas</h1>
  <?php 
    $entradaVista = conseguirEntradas($link);
    if (!empty($entradaVista)) :
      while ($entrada = mysqli_fetch_assoc($entradaVista)):
  ?>
  <article class="entrada">
    <h2><?=$entrada['titulo'];?></h2>
    <p id="fecha"><?=$entrada['categorias'].' | '.$entrada['fecha']?></p>
    <!-- funcion substr limita el numero de caraceteres que tiene un parrafo iniciando en cero y terminando en el numero que le digas -->
    <p><?=substr($entrada['descripcion'], 0, 200)?></p>
  </article>
  <?php
    endwhile;
  endif;
  ?>
  <div id="ver_todas">
    <a href="">Ver más</a>
  </div>
</div>

<?php require_once("includes/footer.php") ?>