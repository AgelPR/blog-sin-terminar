<?php require_once("config.php"); ?>
<?php require_once("includes/funciones.php") ?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="assets/css/main.css">
  <title>Blog</title>
</head>

<body>
  <!-- CABECERA / LOGO-->
  <header id="cabecera">
    <div id="logo">
      <a href="index.php">
        Blog Personal
      </a>
    </div>
    <!-- MENU -->
    <nav id="menu">
      <ul>
        <li><a href="index.php">Inicio</a></li>
        <?php 
          $categoriaVista = conseguirCategorias($link);
          if (!empty($categoriaVista)):
            while($categoria = mysqli_fetch_assoc($categoriaVista)):
        ?>
        <li>
          <a href="categoria.php?id=<?=$categoria['id']?>"><?=$categoria['nombre']?></a>
        </li>
        <?php 
          endwhile;
        endif;
        ?>
        <li><a href="#">Sobre nosotros</a></li>
        <li><a href="#">Contacto</a></li>
      </ul>
    </nav>
  </header>
  <div id="contenedor">