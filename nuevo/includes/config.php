<?php
$server = 'localhost';
$user = 'root';
$pass = '';
$dbName = 'blog_master';

$link = mysqli_connect($server, $user, $pass, $dbName);
mysqli_query($link, 'SET NAMES "utf8"');
if ($link == false) {
  echo "Conection failed: " . mysqli_error($link);
}
if (!isset($_SESSION)) {
  session_start();
}