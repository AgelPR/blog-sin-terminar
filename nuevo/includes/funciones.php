<?php
function mostrarError($errores, $campo)
{
  $alerta = '';
  if ($errores[$campo] && !empty($campo)) {
    $alerta = "<div class='alerta-error'>" . $errores[$campo] . "</div>";
  }
  return $alerta;
}

// Funcion para borrar errores
function borrarErrores()
{
  $resultado = '';
  if (isset($_SESSION['mostrar_errores'])) {
    $_SESSION['mostrar_errores'] = null;
    $resultado = session_unset();
  }

  if (isset($_SESSION['registro_completado'])) {
    $_SESSION['registro_completado'] = null;
    $resultado = session_unset();
  }
  return $resultado;
}
// Funcion para traer consultas de categorias
function conseguirCategorias($conexion)
{
// Realizo la consulta ordenandola por id
  $query = "SELECT * FROM categorias ORDER BY id ASC";
  $categorias = mysqli_query($conexion, $query);
  $result = array();
// Recorro las lineas
  if ($categorias && mysqli_num_rows($categorias) >= 1) {
    $result = $categorias;
  }
  return $result;
}
// Funcion para recorrer las entradas
function conseguirEntradas($conexion){
  $query = "SELECT e.*, c.nombre AS 'categorias' FROM entradas e INNER JOIN categorias c ON e.categoria_id = c.id ORDER BY e.id DESC LIMIT 5";
  $entradas = mysqli_query($conexion, $query);

  $result = array();
// Recorro las lineas
  if ($entradas && mysqli_num_rows($entradas) >= 1) {
    $result = $entradas;
  }
  return $result;
  
}