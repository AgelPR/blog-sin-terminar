<aside id="sidebar">
  <?php if (isset($_SESSION['usuario'])) : ?>
  <div id="login_usuario" class="sesion">
    <p>Hola <?= $_SESSION['usuario']['nombre'] . ' ' . $_SESSION['usuario']['apellido'] ?></p><br>
    <!-- Botones -->
    <a href="crear_entrada.php" class="boton_entrada">Crear entradas</a>
    <a href="crear_categoria.php" class="boton_categoria">Crear categorias</a>
    <a href="cierre_sesion.php" class="boton_perfil">Perfil</a>
    <a href="cierre_sesion.php" class="boton_cierre_sesion">Salir</a>
  </div>
  <?php endif; ?>

  <?php if (!isset($_SESSION['usuario'])) : ?>
  <div id="login" class="bloque">
    <h3>Inicia sesión</h3>
    <?php if (isset($_SESSION['error_login'])) : ?>
    <div class="alerta alerta-error">
      <?= $_SESSION['error_login'] ?>
    </div>
    <?php endif; ?>
    <form action="login.php" method="POST">
      <label for="email">Email</label>
      <input type="email" name="email">

      <label for="password">Contraseña</label>
      <input type="password" name="password">

      <input type="submit" value="Ingresar" name="login_btn">
    </form>
  </div>

  <div id="register" class="bloque">
    <h3>Registrate</h3>
    <?php if (isset($_SESSION['registro_completado'])) : ?>
    <?php
      echo "<div class='alerta'>" . $_SESSION['registro_completado'] . "</div>";
      ?>
    <?php elseif (isset($_SESSION['errores']['general'])) : ?>
    <?php
      echo "<div class='alerta alerta-error'>" . $_SESSION['errores']['general'] . "</div>";
      ?>
    <?php endif; ?>
    <form action="registro.php" method="POST">
      <label for="nombre">Nombre</label>
      <input type="text" name="nombre">
      <?php echo isset($_SESSION['mostrar_errores']) ? mostrarError($_SESSION['mostrar_errores'], 'nombre') : ''; ?>

      <label for="apellido">Apellido</label>
      <input type="text" name="apellido">
      <?php echo isset($_SESSION['mostrar_errores']) ? mostrarError($_SESSION['mostrar_errores'], 'apellido') : ''; ?>

      <label for="email">Email</label>
      <input type="email" name="email">
      <?php echo isset($_SESSION['mostrar_errores']) ? mostrarError($_SESSION['mostrar_errores'], 'email') : ''; ?>

      <label for="password">Contraseña</label>
      <input type="password" name="password">
      <?php echo isset($_SESSION['mostrar_errores']) ? mostrarError($_SESSION['mostrar_errores'], 'clave') : ''; ?>

      <input type="submit" value="Registrar" name=register_btn">
    </form>
    <?php borrarErrores(); ?>
  </div>
  <?php endif;?>
</aside>