<?php
if (isset($_POST)) {
  require_once("includes/config.php");
  // Tomo los datos que pasan por input
  $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : false;
  $apellido = isset($_POST['apellido']) ? $_POST['apellido'] : false;
  $email = isset($_POST['email']) ? $_POST['email'] : false;
  $password = isset($_POST['password']) ? $_POST['password'] : false;

  // Arreglo para almacenar los errores
  $errores = array();

  // Valido los datos antes de guardarlos en la base de datos
  // 1.- Sino está vacío
  // 2.- Sino es númerico
  // 3.- Sino posee expresiones regulares
  if (!empty($nombre) && !is_numeric($nombre) && !preg_match('/[0-9]/', $nombre)) {
    $nombre_validado = true;
  } else {
    $nombre_validado = false;
    $errores['nombre'] = 'Ingresa un nombre valido';
  }

  if (!empty($apellido) && !is_numeric($apellido) && !preg_match('/[0-9]/', $apellido)) {
    $apellido_validado = true;
  } else {
    $apellido_validado = false;
    $errores['apellido'] = 'Ingresa un apellido valido';
  }

  if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $email_validado = true;
  } else {
    $email_validado = false;
    $errores['email'] = 'Por favor ingresa de nuevo el correo';
  }

  if (!empty($password)) {
    $password_validado = true;
  } else {
    $password_validado = false;
    $errores['clave'] = 'La contraseña no puede estar vacía';
  }

  // Cuento los errores antes de hacer un registro en la base de datos
  $guardar_usuario = false;
  if (count($errores) == 0) {
    $guardar_usuario = true;
    // Cifrar contraseña
    $hash = password_hash($password, PASSWORD_BCRYPT, ['cost' => 9]);
    // Sino hay errores se realiza el query
    $query = "INSERT INTO usuarios VALUES(null, '$nombre', '$apellido', '$email', '$hash', CURDATE())";
    $result = mysqli_query($link, $query);

    if (isset($result)) {
      $_SESSION["registro_completado"] = "Registro guardado con éxito";
    } else {
      $_SESSION["errores"]["general"] = "Falló al guardar el registro";
    }
  } else {
    $_SESSION['mostrar_errores'] = $errores;
  }
}
header('Location:index.php');
