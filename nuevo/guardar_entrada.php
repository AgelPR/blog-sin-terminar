<?php
if (isset($_POST)) {
  require_once("includes/config.php");
  $titulo = isset($_POST['newEntrada']) ? mysqli_real_escape_string($link, $_POST['newEntrada']) : false;
  $descripcion = isset($_POST['newDescripcion']) ? mysqli_real_escape_string($link, $_POST['newDescripcion']) : false;
  $categoria = isset($_POST['categoria']) ?   (int)$_POST['categoria'] : false;
  $usuario = $_SESSION['usuario']['id'];

  $errores = array();

  if (empty($titulo)) {
    $errores['titulo'] = 'Por favor, indique el título de la entrada';
  }
  if (empty($descripcion)) {
    $errores['descripcion'] = 'Este campo es obligatorio';
  }
  if (empty($categoria) && !is_numeric($categoria)) {
    $errores['categoria'] = 'La categoria no es valida';
  }

  if (count($errores) == 0) {
    $query = "INSERT INTO entradas VALUES(null, '$usuario', '$categoria', '$titulo', '$descripcion', CURDATE())";
    $result = mysqli_query($link, $query);
  } else {
    $_SESSION['errores_entrada'] = $errores;
    header("Location:crear_entrada.php");
  }
}
header("Location: index.php");