-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 16-05-2021 a las 19:46:14
-- Versión del servidor: 5.7.31
-- Versión de PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `blog_master`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

DROP TABLE IF EXISTS `categorias`;
CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`) VALUES
(1, 'Deportes'),
(2, 'Viajes'),
(3, 'Gastronomia'),
(4, 'Tecnologias'),
(5, 'IoT'),
(6, 'Agricultura'),
(7, 'Cosmeticos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entradas`
--

DROP TABLE IF EXISTS `entradas`;
CREATE TABLE IF NOT EXISTS `entradas` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(100) NOT NULL,
  `categoria_id` int(100) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descripcion` mediumtext,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_entrada_usuario` (`usuario_id`),
  KEY `fk_entrada_categoria` (`categoria_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `entradas`
--

INSERT INTO `entradas` (`id`, `usuario_id`, `categoria_id`, `titulo`, `descripcion`, `fecha`) VALUES
(1, 1, 1, 'El Beisbol Venezolano', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque hic nobis corrupti et consequatur placeat vero vitae incidunt officia! Animi incidunt consequuntur est eius explicabo soluta quo doloribus eum quae aliquam, quam dignissimos consequatur? Culpa, eum rem. Voluptate eum accusamus unde cumque molestias accusantium ad culpa incidunt libero at, nobis repellat, maxime eligendi iste error obcaecati excepturi quidem doloremque mollitia? Ducimus, saepe optio quisquam reprehenderit, libero, amet sit tenetur suscipit eaque molestiae modi. Soluta recusandae, aliquid unde amet placeat asperiores laboriosam eum fugit dignissimos sunt voluptates natus laborum autem quasi commodi, quia dolorem, enim molestias. Voluptate neque exercitationem expedita explicabo!', '2021-02-04'),
(2, 1, 2, 'Mérida la ciudad de los caballeros', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque hic nobis corrupti et consequatur placeat vero vitae incidunt officia! Animi incidunt consequuntur est eius explicabo soluta quo doloribus eum quae aliquam, quam dignissimos consequatur? Culpa, eum rem. Voluptate eum accusamus unde cumque molestias accusantium ad culpa incidunt libero at, nobis repellat, maxime eligendi iste error obcaecati excepturi quidem doloremque mollitia? Ducimus, saepe optio quisquam reprehenderit, libero, amet sit tenetur suscipit eaque molestiae modi. Soluta recusandae, aliquid unde amet placeat asperiores laboriosam eum fugit dignissimos sunt voluptates natus laborum autem quasi commodi, quia dolorem, enim molestias. Voluptate neque exercitationem expedita explicabo!', '2021-02-04'),
(3, 1, 3, 'Platos típicos de Venezuela', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque hic nobis corrupti et consequatur placeat vero vitae incidunt officia! Animi incidunt consequuntur est eius explicabo soluta quo doloribus eum quae aliquam, quam dignissimos consequatur? Culpa, eum rem. Voluptate eum accusamus unde cumque molestias accusantium ad culpa incidunt libero at, nobis repellat, maxime eligendi iste error obcaecati excepturi quidem doloremque mollitia? Ducimus, saepe optio quisquam reprehenderit, libero, amet sit tenetur suscipit eaque molestiae modi. Soluta recusandae, aliquid unde amet placeat asperiores laboriosam eum fugit dignissimos sunt voluptates natus laborum autem quasi commodi, quia dolorem, enim molestias. Voluptate neque exercitationem expedita explicabo!', '2021-02-04'),
(4, 2, 1, 'Juegos típicos de Venezuela', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque hic nobis corrupti et consequatur placeat vero vitae incidunt officia! Animi incidunt consequuntur est eius explicabo soluta quo doloribus eum quae aliquam, quam dignissimos consequatur? Culpa, eum rem. Voluptate eum accusamus unde cumque molestias accusantium ad culpa incidunt libero at, nobis repellat, maxime eligendi iste error obcaecati excepturi quidem doloremque mollitia? Ducimus, saepe optio quisquam reprehenderit, libero, amet sit tenetur suscipit eaque molestiae modi. Soluta recusandae, aliquid unde amet placeat asperiores laboriosam eum fugit dignissimos sunt voluptates natus laborum autem quasi commodi, quia dolorem, enim molestias. Voluptate neque exercitationem expedita explicabo!', '2021-02-04'),
(5, 2, 2, 'Roma y sus calles', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque hic nobis corrupti et consequatur placeat vero vitae incidunt officia! Animi incidunt consequuntur est eius explicabo soluta quo doloribus eum quae aliquam, quam dignissimos consequatur? Culpa, eum rem. Voluptate eum accusamus unde cumque molestias accusantium ad culpa incidunt libero at, nobis repellat, maxime eligendi iste error obcaecati excepturi quidem doloremque mollitia? Ducimus, saepe optio quisquam reprehenderit, libero, amet sit tenetur suscipit eaque molestiae modi. Soluta recusandae, aliquid unde amet placeat asperiores laboriosam eum fugit dignissimos sunt voluptates natus laborum autem quasi commodi, quia dolorem, enim molestias. Voluptate neque exercitationem expedita explicabo!', '2021-02-04'),
(6, 2, 3, 'Masas madres', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque hic nobis corrupti et consequatur placeat vero vitae incidunt officia! Animi incidunt consequuntur est eius explicabo soluta quo doloribus eum quae aliquam, quam dignissimos consequatur? Culpa, eum rem. Voluptate eum accusamus unde cumque molestias accusantium ad culpa incidunt libero at, nobis repellat, maxime eligendi iste error obcaecati excepturi quidem doloremque mollitia? Ducimus, saepe optio quisquam reprehenderit, libero, amet sit tenetur suscipit eaque molestiae modi. Soluta recusandae, aliquid unde amet placeat asperiores laboriosam eum fugit dignissimos sunt voluptates natus laborum autem quasi commodi, quia dolorem, enim molestias. Voluptate neque exercitationem expedita explicabo!', '2021-02-04'),
(7, 3, 1, 'NBA', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque hic nobis corrupti et consequatur placeat vero vitae incidunt officia! Animi incidunt consequuntur est eius explicabo soluta quo doloribus eum quae aliquam, quam dignissimos consequatur? Culpa, eum rem. Voluptate eum accusamus unde cumque molestias accusantium ad culpa incidunt libero at, nobis repellat, maxime eligendi iste error obcaecati excepturi quidem doloremque mollitia? Ducimus, saepe optio quisquam reprehenderit, libero, amet sit tenetur suscipit eaque molestiae modi. Soluta recusandae, aliquid unde amet placeat asperiores laboriosam eum fugit dignissimos sunt voluptates natus laborum autem quasi commodi, quia dolorem, enim molestias. Voluptate neque exercitationem expedita explicabo!', '2021-02-04'),
(8, 3, 2, 'Viaje al centro de la tierra', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque hic nobis corrupti et consequatur placeat vero vitae incidunt officia! Animi incidunt consequuntur est eius explicabo soluta quo doloribus eum quae aliquam, quam dignissimos consequatur? Culpa, eum rem. Voluptate eum accusamus unde cumque molestias accusantium ad culpa incidunt libero at, nobis repellat, maxime eligendi iste error obcaecati excepturi quidem doloremque mollitia? Ducimus, saepe optio quisquam reprehenderit, libero, amet sit tenetur suscipit eaque molestiae modi. Soluta recusandae, aliquid unde amet placeat asperiores laboriosam eum fugit dignissimos sunt voluptates natus laborum autem quasi commodi, quia dolorem, enim molestias. Voluptate neque exercitationem expedita explicabo!', '2021-02-04'),
(9, 3, 3, 'La arepa como simbolo del Pais', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque hic nobis corrupti et consequatur placeat vero vitae incidunt officia! Animi incidunt consequuntur est eius explicabo soluta quo doloribus eum quae aliquam, quam dignissimos consequatur? Culpa, eum rem. Voluptate eum accusamus unde cumque molestias accusantium ad culpa incidunt libero at, nobis repellat, maxime eligendi iste error obcaecati excepturi quidem doloremque mollitia? Ducimus, saepe optio quisquam reprehenderit, libero, amet sit tenetur suscipit eaque molestiae modi. Soluta recusandae, aliquid unde amet placeat asperiores laboriosam eum fugit dignissimos sunt voluptates natus laborum autem quasi commodi, quia dolorem, enim molestias. Voluptate neque exercitationem expedita explicabo!', '2021-02-04'),
(10, 5, 4, 'Sistemas Operativos', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque hic nobis corrupti et consequatur placeat vero vitae incidunt officia! Animi incidunt consequuntur est eius explicabo soluta quo doloribus eum quae aliquam, quam dignissimos consequatur? Culpa, eum rem. Voluptate eum accusamus unde cumque molestias accusantium ad culpa incidunt libero at, nobis repellat, maxime eligendi iste error obcaecati excepturi quidem doloremque mollitia? Ducimus, saepe optio quisquam reprehenderit, libero, amet sit tenetur suscipit eaque molestiae modi. Soluta recusandae, aliquid unde amet placeat asperiores laboriosam eum fugit dignissimos sunt voluptates natus laborum autem quasi commodi, quia dolorem, enim molestias. Voluptate neque exercitationem expedita explicabo!', '2021-02-08'),
(11, 22, 6, 'Cosechas en Invierno', 'En estos tiempos, la sequia azota nuestros campos lo cual impide que la producción se retrase impidiendo de esta manera poder despachar los productos en los tiempos establecidos.', '2021-04-20'),
(12, 22, 7, 'Maquillajes para la piel oscura', 'aksjnaspjbfpasnpasbnpoasbdvibjasdpuibvd', '2021-04-21'),
(13, 22, 1, '', '', '2021-04-21'),
(14, 22, 1, '', '', '2021-04-21');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `entradas_autor`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `entradas_autor`;
CREATE TABLE IF NOT EXISTS `entradas_autor` (
`titulo` varchar(255)
,`descripcion` mediumtext
,`Autor` varchar(255)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellido`, `email`, `password`, `fecha`) VALUES
(1, 'Agel', 'Puentes', 'angelpuentes21@gmail.com', '1234', '2020-02-01'),
(2, 'Desire', 'Peralta', 'desireperalta@gmail.com', '1234', '2020-02-01'),
(3, 'Yusbely', 'Puentes', 'test21@test.com', '123', '2021-02-01'),
(4, 'Alfonso', 'Puentes', 'test121@test.com', '123', '2021-02-01'),
(5, 'Isabel', 'Rodriguez', 'test1211@test.com', '123', '2021-02-01'),
(6, 'Rene', 'Rodriguez', 'cualquiera@gmail.com', '12343', '2018-01-23'),
(7, 'Jesus', 'Puentes', 'cualquierasda@gmail.com', '12343', '2017-01-13'),
(9, 'Ingrid', 'Rodriguez', 'ingrid@gmail.com', '12343', '2017-01-23'),
(16, 'Script', 'Script', 'script@gmail.com', '$2y$09$NyTx6oVV6vL.dL085iwpue8gaNAjqWP7QlM5EzKVf1RXwt3Hi.iBy', '2021-04-18'),
(17, 'prueba', 'prueba', 'prueba@prueba.com', '$2y$09$ev6RXNWHZCROX2oXqcP29O7txiGRaQe.yLRhbT1m2SnCGIo/6ab3i', '2021-04-18'),
(21, 'bancamiga', 'bancamiga', 'banca@bancamiga.com', '$2y$09$CQK6FTZifmpugejPg2XgNe7HQXD.wsLJlXk.HMBbtz/irk5BB0WLK', '2021-04-18'),
(22, 'German ', 'Peralta', 'germanperalta@gmail.com', '$2y$09$b0DQUAywmTN4eb77HE1m4u1WK0fIJWhqlLdNhjyqILHAbpVcRfnZq', '2021-04-19'),
(23, 'Maria Neris', 'Nieto', 'marianieto@gmail.com', '$2y$09$3q3Xtq0nanm6UdlrcKrvl.HzdSjUTpQtm3gPxTRdZhTky.WE/aD9K', '2021-04-19'),
(24, 'Administrador', 'Administrador', 'admin@admin.com', '$2y$09$w1b8iTcoXZKQC7av21hEwuV8oA1yZKzdoro9XHO2YnFA0TdYQteaK', '2021-04-20');

-- --------------------------------------------------------

--
-- Estructura para la vista `entradas_autor`
--
DROP TABLE IF EXISTS `entradas_autor`;

DROP VIEW IF EXISTS `entradas_autor`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `entradas_autor`  AS  select `e`.`titulo` AS `titulo`,`e`.`descripcion` AS `descripcion`,`u`.`nombre` AS `Autor` from (`entradas` `e` join `usuarios` `u` on((`e`.`categoria_id` = `u`.`id`))) ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `entradas`
--
ALTER TABLE `entradas`
  ADD CONSTRAINT `fk_entrada_categoria` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_entrada_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
